<h1>Tide Table Client Settings</h1>
<br />
<form method="POST">

<label for="fname">API URL:</label>
<input type="text" id="api_url" name="api_url" value = "<?= $payload['api_url'] ?? '';?>"><br><br>
<p>This is the full URL to the API's Endpoint. e.g. https://example.com/wp-json/eicc/tide-tables/v1/tides</p>

<label for="fname">API User Name:</label>
<input type="text" id="api_user_name" name="api_user_name" value = "<?= $payload['api_user_name'] ?? '';?>"><br><br>
<p>This is the name of the user that you created the application password on.</p>

<label for="fname">API Password:</label>
<input type="text" id="api_password" name="api_password" value = "<?= $payload['api_password'] ?? '';?>"><br><br>

<input type="submit" name="tide_table_submit" value="Submit">
</form>
