<?php

/**
 * This is the client for the tide-table-api project.
 *
 * This WordPress plugin makes a call to a website running tide-table-api.
 */
class Plugin
{
  const OPTIONS_PAGE = 'Templates/admin_page.php';
  const OPTIONS_KEY = 'tide_client_options';

  protected string $pluginDir = '';
  protected string $pluginUrl = '';

  public function __construct()
  {
      $this->pluginDir = plugin_dir_path(__FILE__);
      $this->pluginUrl = plugin_dir_url(__FILE__);
  }

   /**
    * Setup things
    */
  public function init(): void
  {
    \add_shortcode('tide_tables', [$this,'displayShortcode']);
    \add_shortcode('tide_table_image', [$this,'fetchImageTag']);
    \add_shortcode('tide_table_url', [$this,'fetchImageUrl']);
    \add_action('admin_menu', [ $this, 'adminMenu' ], 11);
  }

  /**
   * Register the admin menu under Settings
   */
  public function adminMenu(): void
  {
    \add_submenu_page(
        'options-general.php',
        'Tide Tables Client Settings',
        'Tide Tables Client',
        'manage_options',
        'tide_tables_admin',
        [ $this, 'displayAdminPage' ]
    );
  }

  /**
   * Display the admin page and handle populating the variables passed in.
   */
  public function displayAdminPage(): void
  {
    $optionKey = 'tide_client_options';

    if (
        !empty($_POST)
        && isset($_POST['tide_table_submit'])
        && $_POST['tide_table_submit'] === 'Submit'
    ) {
      $payload = [];
      $payload['api_url'] = filter_var($_POST['api_url'], FILTER_SANITIZE_STRING);
      $payload['api_user_name'] = filter_var($_POST['api_user_name'], FILTER_SANITIZE_STRING);
      $payload['api_password'] = filter_var($_POST['api_password'], FILTER_SANITIZE_STRING);
      update_option(self::OPTIONS_KEY, json_encode($payload));
    }

    $payload = json_decode(get_option(self::OPTIONS_KEY), true);
    include $this->pluginDir . self::OPTIONS_PAGE;
  }

  /**
   * The heart of the client system.  This is called when WordPress
   * encounters a shortcode.
   *
   * [tide_tables station=? days=? format=? name=?]
   */
  public function displayShortcode($parameters): string
  {
    $parameters = shortcode_atts(
        ['station' => '','days' => 30,'format' => 'table','header' => 1,'name' => ''],
        $parameters
    );
    $parameters['header'] = (bool)$parameters['header'];

    $options = json_decode(get_option(self::OPTIONS_KEY), true);

    if ($parameters['days'] < 1 || $parameters['days'] > 45) {
      return "<h2>Cannot display " . $parameters['days'] . " days of data.</h2>";
    }

    $rowTemplateFile = $this->pluginDir . 'Templates/' . $parameters['format'] . '/date_row.html';

    if (! file_exists($rowTemplateFile)) {
      wp_die($parameters['format'] . ' is an invalid format.');
    }

    $startDate = new \DateTimeImmutable("now", new DateTimeZone((wp_timezone())->getName()));
    wp_enqueue_style('tide-table-client', $this->pluginUrl . 'Templates/' . $parameters['format']  . '/style.css');

    /*
     * css file in the theme dir can override the default css shipped with the plugin
     * file names MUST be tide-table-client-[card|table].css
     */
    $themeSpecificStyleUrl = get_stylesheet_directory_uri() . '/tide-table-client-' . $parameters['format']  . '.css';
    $themeSpecificStyleFile = get_stylesheet_directory() . '/tide-table-client-' . $parameters['format']  . '.css';

    if (file_exists($themeSpecificStyleFile)) {
      wp_enqueue_style('tide-table-client-theme', $themeSpecificStyleUrl);
    }

    $rowTemplate = file_get_contents($rowTemplateFile);

    try {
      $payload = $this->fetchDate($startDate->format('Y-m-d'), $parameters['station'], $options);
      $station = $payload['station'];
      $station['name'] = !empty($parameters['name']) ? $parameters['name'] : $station['name'];
    } catch (\Exception $e) {
      error_log($e->getMessage());
      return '<h1>Error reading the station data from the API</h1>';
    }

    $status = (int)($payload['data']['status'] ?? 200);

    if ($status !== 200) {
      error_log($status . ' : ' . $payload['code']);
      return '<h1>Error reading the station data from the API</h1>';
    }

    $startDate = new \DateTimeImmutable("now", new DateTimeZone($station['timezone']));

    $dataTable = '';

    foreach ($this->createEmptyDataArray($startDate, $parameters['days']) as $date) {
      try {
        $dataTable .= $this->buildRow(
            $rowTemplate,
            $this->fetchDate($date, $parameters['station'], $options)['dates']
        );
      } catch (\Exception $e) {
        error_log($e->getMessage());
      } catch (\Error $e) {
        error_log($e->getMessage());
         return '<h1>Error generating the output.</h1>';
      }
    }

    return $this->mergeTemplates(
        $dataTable,
        $station,
        $parameters['format'],
        $parameters['header']
    );
  }

  /**
   * Fetch a graphic of a single date
   */
  public function fetchImage($parameters): string
  {
    include_once(ABSPATH . 'wp-admin/includes/image.php');

    /*
     * Handle TODAY and TOMORROW
     */
    if (isset($parameters['date']) && trim(strtolower($parameters['date'])) === 'today') {
      $parameters['date'] = (new \DateTimeImmutable())->format('Y-m-d');
    }

    if (isset($parameters['date']) && trim(strtolower($parameters['date'])) === 'tomorrow') {
      $parameters['date'] = (new \DateTimeImmutable())->add(new DateInterval('P1D'))->format('Y-m-d');
    }

    $parameters = shortcode_atts(
        ['station' => '','days' => 1,'format' => 'instagram','date' => date('Y-m-d'),'name' => '','ignoreCache' => false],
        $parameters
    );

    $parameters['ignoreCache'] = (bool)$parameters['ignoreCache'];
    $parameters['days'] = 1;

    $options = json_decode(get_option(self::OPTIONS_KEY), true);

    /*
     * Init parameters
     */
    $startDate = new \DateTimeImmutable($parameters['date']);
    $station = $this->fetchDate($startDate->format('Y-m-d'), $parameters['station'], $options)['station'];
    $name = $parameters['name'] ?? $station['name'];

    /*
     * Check to see if it already exists
     */
    $uploadDirArray = wp_upload_dir();

    $fileName = strtr($name, ' ', '_') . '_' . $startDate->format('Y_m_d') . '.png';
    $newFilePathName = $uploadDirArray['path'] . '/' . $fileName;
    $fileExists = file_exists($newFilePathName);

    if (
        $fileExists &&
        filemtime($newFilePathName) < (new \DateTimeImmutable())->modify('-1 day')->format('U')
    ) {
      unlink($newFilePathName);
      $fileExists = false;
    }

    if ($fileExists && ! $parameters['ignoreCache']) {
      return $uploadDirArray['url'] . '/' . $fileName;
    }

    $apiParams = [
      'format' => $parameters['format'],
      'date' => $startDate->format('Y-m-d'),
      'days' => $parameters['days'],
      'stationId' => $parameters['station'],
      'name' => $name
    ];

    $image = $this->curlGet(
        $options['api_url'] . '?format=instagram&',
        [
          'user' => $options['api_user_name'],
          'password' => $options['api_password']
        ],
        $apiParams
    );

    file_put_contents($newFilePathName, $image);

    $mimeType = mime_content_type($newFilePathName);
    $newUrl = $uploadDirArray['url'] . '/' . $fileName;

    $upload_id = wp_insert_attachment(array(
      'guid'           => $newUrl,
      'post_mime_type' => $mimeType,
      'post_title'     => $fileName,
      'post_content'   => '',
      'post_status'    => 'inherit'
    ), $newFilePathName);

    $upload_id = $this->findImageId($uploadDirArray['url'] . '/' . $fileName);
    \wp_update_attachment_metadata(
        $upload_id,
        \wp_generate_attachment_metadata($upload_id, $newFilePathName)
    );

    return $newUrl;
  }

  /**
   *  Returns just the URL.
   */
  public function fetchImageUrl($parameters): string
  {
    if (isset($_GET['date'])) {
      $parameters[0]['date'] = (new \DateTimeImmutable($_GET['date']))->format('Y-m-d');
    }

    return $this->fetchImage($parameters);
  }

    /**
     *  Returns the full image tag
     */
  public function fetchImageTag($parameters): string
  {
    if (isset($_GET['date'])) {
      $parameters['date'] = (new \DateTimeImmutable($_GET['date']))->format('Y-m-d');
    }

    $url = $this->fetchImage($parameters);
    return '<img src="' . $url . '" />';
  }

  /**
   * Using the URL of the image, it finds the image ID  and returns it.
   */
  protected function findImageId($image_url)
  {
    global $wpdb;
    $attachment = $wpdb->get_col(
        $wpdb->prepare(
            "SELECT ID FROM $wpdb->posts WHERE guid='%s';",
            $image_url
        )
    );

    return $attachment[0];
  }

  /**
   * Each time you add a data item to the table row, you need to add a
   * str_replace here to populate it.
   */
    /**
   * Each time you add a data item to the table row, you need to add a
   * str_replace here to populate it.
   */
  protected function buildRow(string $template, array $data): string
  {
    $date = array_keys($data)[0];
    $today = new \DateTimeImmutable();
    $rowDate = (new \DateTimeImmutable($date));
    $returnValue = $template;
    if ($today->format('D m-d-Y') === $rowDate->format('D m-d-Y')) {
      $returnValue = str_replace('<!--TODAYSDATE-->', 'todaysDate', $returnValue);
    }
    $returnValue = str_replace('<!--DATE-->', $rowDate->format('D m-d-Y'), $returnValue);
    $returnValue = str_replace('<!--SUNRISE-->', $data[$date]['ipgeo']['sunrise'], $returnValue);
    $returnValue = str_replace('<!--SUNSET-->', $data[$date]['ipgeo']['sunset'], $returnValue);

    try {
      $returnValue = str_replace('<!--MOONRISE-->', (new \DateTimeImmutable($data[$date]['ipgeo']['moonrise']))->format('h:i A'), $returnValue);
      $returnValue = str_replace('<!--MOONSET-->', (new \DateTimeImmutable($data[$date]['ipgeo']['moonset']))->format('h:i A'), $returnValue);
    } catch (\Exception $e) {
      // NOOP
    }

    $returnValue = str_replace('<!--HIGHTIDE_AM-->', $data[$date]['tides']['high']['am']['time'] ?? '--', $returnValue);
    $returnValue = str_replace('<!--HIGHTIDE_PM-->', $data[$date]['tides']['high']['pm']['time'] ?? '--', $returnValue);
    $returnValue = str_replace('<!--MOON_ICON_URL-->', $this->pluginUrl . 'img/moon/' . $data[$date]['moon']['index'] . '.png', $returnValue);
    $returnValue = str_replace('<!--MOON_PHASE-->', $data[$date]['moon']['phase'], $returnValue);
    $moonImageUrl = $this->pluginUrl . '/img/moon/' . $data[$date]['moon']['index'] . '.png';
    $returnValue = str_replace('<!--MOON_IMAGE_URL-->', $moonImageUrl, $returnValue);

    if (isset($data[$date]['weather']) && ! empty($data[$date]['weather'])) {
      $weatherImage = '<img src="' . $this->pluginUrl . 'img/weather/' . $data[$date]['weather']['weather']['icon'] . '.png' . '" alt="weather" width="32" height="32"/>';
      $weatherDescription = $data[$date]['weather']['weather']['description'];
    } else {
      $weatherImage = '&nbsp;<!--NO FORECAST -->';
      $weatherDescription = '&nbsp;<!--NO FORECAST -->';
    }

    $returnValue = str_replace('<!--WEATHER_IMAGE-->', $weatherImage, $returnValue);
    $returnValue = str_replace('<!--WEATHER_DESCRIPTION-->', $weatherDescription, $returnValue);

    $atrributes = '';

    /*
     * Weather Icon
     */
    if (isset($data[$date]['weather']) && ! empty($data[$date]['weather'])) {
      $atrributes .= '<img src="' . $this->pluginUrl . 'img/weather/' . $data[$date]['weather']['weather']['icon'] . '.png' . '" title="' . $data[$date]['weather']['weather']['description'] . '" width="32" height="32"/>';
    }

    /*
     * Night Dive Icon
     */
    if ($data[$date]['tides']['high']['pm']['time'] !== '--') {
    $highTidePM = new \DateTimeImmutable($date . $data[$date]['tides']['high']['pm']['time'].'PM');
    $sunset = new \DateTimeImmutable($date. $data[$date]['ipgeo']['sunset'].'PM');
    $parkClose = new \DateTimeImmutable(  (new \DateTimeImmutable($date. $data[$date]['ipgeo']['sunset']))->format('D Y-m-d') . '22:00' );

    if (
      $highTidePM < $parkClose
      && $highTidePM > $sunset
      && $highTidePM->add(new DateInterval('PT45M')) <= $parkClose
      ) {
        $moonIconUrl = $this->pluginUrl . 'img/moon/15.png';
        $moonIconFile = $this->pluginDir . 'img/moon/15.png';


        if (file_exists($moonIconFile)) {
          $atrributes .= '<img src="' . $moonIconUrl . '" alt="" width="32" title="Potential good night dive" height="32"/>';
        }
      }
    }


    $returnValue = str_replace('<!--ATTRIBUTES-->', $atrributes, $returnValue);

    return $returnValue;
  }


  protected function mergeTemplates(
      string $dataTable,
      array $station,
      string $displayFormat,
      bool $displayHeader
  ): string {
    $masterTemplate = file_get_contents($this->pluginDir . 'Templates/' . $displayFormat . '/master.html');
    if ($displayHeader) {
        $header = "
      <p>
        <h3><!--STATION_NAME--></h3>
        <strong>NOAA Station Id :<!--STATION_ID--></strong>
      </p>";
        $header = str_replace('<!--STATION_NAME-->', $station['name'], $header);
        $header = str_replace('<!--STATION_ID-->', $station['id'], $header);
    } else {
        $header = '';
    }

      $returnValue = str_replace('<!--HEADER-->', $header, $masterTemplate);
      $returnValue = str_replace('<!--TABLE-->', $dataTable, $returnValue);

      return $returnValue;
  }

  /*
  * Fetch One Day's data
  *
  * Looks for the data in a transient first. If not there then it hits the API.
  */
  protected function fetchDate(
      string $date,
      string $stationId,
      array $options
  ): array {
      $date = new \DateTimeImmutable($date);
      $transientKey = 'tide_client_'  . $stationId . '_' . $date->format('Ymd');
      $transientData = get_transient($transientKey);

    if (! $transientData) {
      $apiParams = [
        'stationId' => $stationId,
        'days' => 1,
        'date' => $date->format('Y-m-d')
      ];

      $transientData = json_decode(
          $this->curlGet(
              $options['api_url'],
              ['user' => $options['api_user_name'],'password' => $options['api_password']],
              $apiParams
          ),
          true
      );

      /*
      * Bypass saving if error
      */
      $status = (int)($transientData['data']['status'] ?? 200);

      if ($status !== 200) {
        throw new \Exception('ERROR READING DATA');
      }

      if (empty($transientData)) {
        throw new \Exception('NO DATA RETURNED FROM THE API');
      }

      $current = new DateTimeImmutable();
      $currentUTC = $current->format('U');
      $midnight = new DateTimeImmutable($current->format('Y-m-d 23:59:59'));

      /*
       * adding 10 seconds because the API endpoints also end at 59:59. Just
       * want to make sure we force a refresh,
       */
      $midnightUTC = $midnight->format('U') + 10;
      $expiry = $midnightUTC - $currentUTC;

      set_transient($transientKey, $transientData, 86400);
    }

    return $transientData;
  }


  /*
  * Support functions
  */
  protected function curlGet($url, array $auth, array $get = [], array $options = array()): string
  {
    $defaults = array(
      CURLOPT_URL => $url . ( strpos($url, '?') === false ? '?' : '' ) . http_build_query($get),
      CURLOPT_HEADER => 0,
      CURLOPT_RETURNTRANSFER => true
    );
    $ch = curl_init();
    curl_setopt_array($ch, ( $options + $defaults ));
    curl_setopt($ch, CURLOPT_USERPWD, $auth['user'] . ":" . $auth['password']);

    if (! $result = curl_exec($ch)) {
      trigger_error(curl_error($ch));
    }
    curl_close($ch);
    return $result;
  }

  /**
   * Creates an array of dates that can then be populated form the API. This way
   * we know that the resulting array has an element for each day requested.
   */
  protected function createEmptyDataArray(
      \DateTimeImmutable $startDate,
      int $days = 30
  ): array {
    $returnValue = [];

    $days = (int)($days - 1 > 0 ? $days - 1 : $days);

    if ($days === 1) {
        $returnValue[] = $startDate->format('Y-m-d');
        return $returnValue;
    }

    $period = new \DatePeriod($startDate, new \DateInterval('P1D'), $days);

    foreach ($period as $currentDate) {
      $returnValue[] = $currentDate->format('Y-m-d');
    }

    return $returnValue;
  }
}
