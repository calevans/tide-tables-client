# Tide Tables Client Plugin<br />
Cal Evans <**cal @ calevans dot com**><br />
(c) 2022 2022 E.I.C.C., Inc.

This is the client for Tide Tables API plugin. This plugin will talk to the API, pull the info for a given station and range of days, and display it.

This plugin requires a username and password from a site that has the API installed. Currently, that site is https://dive.calevans.com. Logins and passwords are free, you just need to contact **cal @ calevans dot com** to get one.

This code is released under the MIT license, a copy of which is included with the package.

# Installation and use
1. Install this plugin just like any other WordPress plugin.
1. Activate the plugin
1. Go to `Settings->Tide Table Client` and set the URL of the site that has the API installed along with the login and password.
1. Add a post with the shorttag [tide_tables station = '8722588' days=30] (8722588 is the NOAA station ID for the Port of West Palm Beach station. Put in any NOAA station ID you want.)
1. Save and then visit that post.

## Images
This plugin can also request an image suitable for display. To display the image in a post, use the `tide_table_image` shorttag.

```
[tide_table_image station='8722588' format='instagram' name='Blue Heron Bridge' date='2022-05-05']
```

Adjust the date to whatever date you want displayed.

You can put a link to download the image by using this piece of code.

```
Download: <a href="[tide_table_url station='8722588' format='instagram' name='Blue Heron Bridge']">[tide_table_url station='8722588' days='1' format='instagram' name='Blue Heron Bridge' date='2022-05-05']</a>
```

# Shortcodes

## `[tide_tables]`
This will display a table of the next X days from the current date.

Options:
- station : The station id to fetch
- days : The number of days to fetch including the date
- format: The format to use. This is either **table** or **card**
- header: 0 = do not display the header text. 1 = display the header text. Default is 1. (true)
- name : Override the official NOAA name for the station with a vanity name. e.g. Instead of 'Port of West Palm Beach', you can set it to 'Blue Heron Bridge'.

## `[tide_table_image]`
- station : The station id to fetch
- date : The date to display. If you ommit this then it displays the current date.
- format : Currently only Instagram is supported.
- name : Override the NOAA Station name
- ignoreCache : Any non false value will ignore the local cache and re-fretch the data from the API.
- name : Override the official NOAA name for the station with a vanity name. e.g. Instead of 'Port of West Palm Beach', you can set it to 'Blue Heron Bridge'.

## `[tide_table_url]`
- station : The station id to fetch
- date : The date to display. If you ommit this then it displays the current date.
- name : Override the official NOAA name for the station with a vanity name. e.g. Instead of 'Port of West Palm Beach', you can set it to 'Blue Heron Bridge'.

# Overriding the default CSS
The CSS for a given format can be overridden by placing a css file in the template directory. It should be named `tide-table-client-FORMAT.css`

e.g.

`tide-table-client-card.css` or `tide-table-client-table.css`
