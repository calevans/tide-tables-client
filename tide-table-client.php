<?php

/*
 * Plugin Name: Tide Tables Client
 * Plugin URI:  https://gitlab.com/calevans/tide-tables-plugin
 * Description: Display tide tables from NOAA
 * Version:     1.5.0
 * Author:      Cal Evans
 * Author URI:  https://calevans.com
 * Text Domain: tide-tables
 * License:     MIT
 */
if (! defined('WPINC')) {
    die();
}

require_once 'src/Plugin.php';

(new Plugin())->init();
