# Development
1. Spin up a tide-tables-API Lando instance
2. In the API instance, create an application password on admin
3. Log into tide-tables-client
4. Go to Settings-Tide Table-Client
5. Make sure the URL is for the API instace (https://tide-table-api.lndo.site/wp-json/eicc/tide-tables/v1/tides)
6. Make sure the user is admin
7. Change the password to the newly created password.
8. Add a post with the shorttag [tide_tables station = '8722588' days=30]
9. Save and then visit that post.
10. To test the image tag add this to a post:

```
[tide_table_image station='8722588' days='1' format='instagram' name='Blue Heron Bridge' date='2022-05-05']
```
11. Create a page "Display Blue Heron Bridge Graphic" and put this in it.

```
[tide_table_image station='8722588' days='1' format='instagram' name='Blue Heron Bridge' date='2022-05-05']

Download: <a href="[tide_table_url station='8722588' days='1' format='instagram' name='Blue Heron Bridge' date='2022-05-05']">[tide_table_url station='8722588' days='1' format='instagram' name='Blue Heron Bridge' date='2022-05-05']</a>
```

That will display the image and give a link to download it.

# TODO

- Refactor Plugin. Move things into discrete classes
  - Creating an image should be in it's own class
  - Creating a table should be in it's own class
  - Creating a table row should be in it's own class
- fetchImage should actually return an image, not a URL to the image. fetchUrl should return the URL.
- Cron task to delete images more than 60 days old
- Admin page should ask user how long to keep old images. Default to 60 days.
